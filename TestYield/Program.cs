﻿using Grpc.Core;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GRPCTest
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var enumerator = new ListOfStrings().GetAsyncEnumerator();
            int counter = 0;
            //Console.WriteLine(GenerateKey());
            //Console.ReadLine();
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            while (await enumerator.MoveNextAsync())
            {
                Console.WriteLine(enumerator.Current);
                counter++;
            }
            stopwatch.Stop();
            Console.WriteLine($"Pobrano {counter} linii w {stopwatch.Elapsed.ToString(@"hh\:mm\:ss")}");
            Console.ReadLine();
        }
        //eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTI3MzM4MzksImlzcyI6InVzZXIiLCJhdWQiOiJhdWRpZW5jZSJ9.So8qkFCiRglHWwVKs0nmWrgMG49Ohdmj-FlOiVUZlbo
        private static string GenerateKey()
        {
            var securityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("this is my private key"));
            var cedentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken("user", "audience", null, expires: DateTime.Now.AddDays(4), signingCredentials: cedentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
    public class ListOfStrings : IAsyncEnumerable<string>
    {
        public async IAsyncEnumerator<string> GetAsyncEnumerator(CancellationToken cancellationToken = default)
        {
            var credentials = CallCredentials.FromInterceptor((c, m) =>
            {
                m.Add("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTM5ODQxMTIsImlzcyI6InVzZXIiLCJhdWQiOiJhdWRpZW5jZSJ9.UyIhN5wByuGiv98mTM636ubyEJ7phHZv5OFpY_FIfe4");
                return Task.CompletedTask;
            });

            var channel = Grpc.Net.Client.GrpcChannel.ForAddress("https://localhost:5001",new Grpc.Net.Client.GrpcChannelOptions
            {
                Credentials = ChannelCredentials.Create(new SslCredentials(), credentials)
            });
            var greedClient = new GrpcService1.Greeter.GreeterClient(channel);
            using (var call = greedClient.SayHello(new GrpcService1.HelloRequest()))
            {
                while (await call.ResponseStream.MoveNext(cancellationToken))
                {
                    yield return call.ResponseStream.Current.Message;
                }
            }
        }
    }
}
