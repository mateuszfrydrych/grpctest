using Grpc.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcService1
{
    [Authorize]
    public class GreeterService : Greeter.GreeterBase
    {
        private readonly ILogger<GreeterService> _logger;
        private List<string> list = new List<string>();
        public GreeterService(ILogger<GreeterService> logger)
        {
            _logger = logger;
            ExpandList();
        }

        private void ExpandList()
        {
            int i = 0;
            while(list.Count < 10000)
            {
                list.Add("Message - " + i);
                i++;
            }
        }

        public override async Task SayHello(HelloRequest request, IServerStreamWriter<HelloReply> responseStream, ServerCallContext context)
        {
           foreach(var msg in list)
            {
                await responseStream.WriteAsync(new HelloReply { Message = msg });
            }
        }
    }
}
